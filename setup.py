from setuptools import setup, find_packages

setup(name='empty_python_package',
      version='0.1',
      description='Folder structure for python project',
      url='https://bitbucket.org/gbellstam/empty_python_package/',
      author='Gustaf',
      author_email='gustaf@bellstam .com',
      license='',
      packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
      install_requires=[],
      zip_safe=False)
