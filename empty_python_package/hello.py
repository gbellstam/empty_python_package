from empty_python_package.hello_world import hello_world
from empty_python_package.sub_package.hello_universe import hello_universe


def hello():
    """
    from empty_python_package.hello import hello
    """
    hello_world()
    hello_universe()
