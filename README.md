## Table of Contents

Minimal python package setup with some import examples for the next time I forget.

[TOC]

## Cloning and creating a new package

#### 1\. Clone

```
git clone git@bitbucket.org:gbellstam/empty_python_package.git
```

#### 2\. Remove .git folder and all contents

```
rm -rf .git
```

#### 4\. Rename folder and source folder

#### 5\. Change name in `setup.py`

#### 6\. Re-initialize

```
git init
git add .
git commit -m "initial commit"
```

#### 7\. Add remote and push

Create repo on bitbucket, github, or whatever

Then add remote, and push, for example

```
git remote add origin https://username@your.bitbucket.domain:7999/new_package_name/repo.git 
git push -u origin master
```

## Installing and importing package

Navigate to folder

Run in terminal:

```
pip install .
```

Open python and test the following command

```python
from empty_python_package.hello import hello
hello()
```

## Setup in vscode

#### 1\. Clone to new folder and rename

Follow instructions in section: [Cloning and creating a new package](#markdown-header-cloning-and-creating-a-new-package)

#### 2\. Open repo folder in vscode and save as workspace

#### 3\. Install a virtual environment

Open terminal and run:

```
python -m venv .venv
```

Accept the "select as environment for workspace" prompt that appears. If it doesn't appear, you can select it with "Python: Select Interpreter" in the search menu.

## Stuff I usually forget

- `find_packages()` in `setup.py` is what makes it possible to import sub-modules and packages
- `__init__.py` in the code folder (i.e. empty_python_package/empty_python_package) makes the imports simpler. Without it, you need `empty_python_package.empty_python_package`
- You can further simplify complicated imports by doing them in the `__init__.py` file
